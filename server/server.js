const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const cors = require('cors');
require('./config/mongoose');
const apiRouter = require('./routes')
const PORT = 5000;

app.use(cors());
app.use(bodyParser.json());
app.use((req, res, next) => {
	res.setHeader('Access-Control-Allow-Origin', '*');
	next();
})
app.use('/api', apiRouter)

app.listen(PORT, function () {
	console.log("Server is running on Port: " + PORT);
});
