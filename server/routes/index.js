const express = require('express')
const checkersRouter = require('./Checkers/Checkers.controller')
const api = new express.Router()

api.use('/checkers', checkersRouter)

module.exports = api
